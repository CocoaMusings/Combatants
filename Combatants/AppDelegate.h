//
//  AppDelegate.h
//  Combatants
//
//  Created by Eric Matecki on 19/12/2017.
//  Copyright © 2017 Eric Matecki. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface  AppDelegate : NSObject <NSApplicationDelegate>

@end

