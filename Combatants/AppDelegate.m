//
//  AppDelegate.m
//  Combatants
//
//  Created by Eric Matecki on 19/12/2017.
//  Copyright © 2017 Eric Matecki. All rights reserved.
//

#import "AppDelegate.h"


extern  void  JustDoIt();


@interface  AppDelegate ()

@end


@implementation  AppDelegate


- (void) applicationDidFinishLaunching: (NSNotification*)iNotification
{
    JustDoIt();
}


- (void) applicationWillTerminate: (NSNotification*)iNotification
{
    // Insert code here to tear down your application
}


@end
