//
//  main.m
//  Combatants
//
//  Created by Eric Matecki on 19/12/2017.
//  Copyright © 2017 Eric Matecki. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
