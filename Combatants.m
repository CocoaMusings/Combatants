/*
 This is a WORK-IN-PROGRESS, and doesn't work right now....
 It's littered with debugging code.

 An implementation of the "Combatants" example, building the GUI and it's bindings entirely in code.
 No NIB/XIB involved (except for the menubar which we don't care about).

 This file is based on code with the following license:

"""
 Version: 2.0

 Disclaimer: IMPORTANT:  This Apple software is supplied to you by  Apple Inc. ("Apple") in consideration of your agreement to the following terms,
 and your use, installation, modification or redistribution of this Apple software constitutes acceptance of these terms.
 If you do not agree with these terms, please do not use, install, modify or redistribute this Apple software.

 In consideration of your agreement to abide by the following terms, and subject to these terms, Apple grants you a personal, non-exclusive license,
 under Apple's copyrights in this original Apple software (the "Apple Software"), to use, reproduce, modify and redistribute the Apple Software,
 with or without modifications, in source and/or binary forms; provided that if you redistribute the Apple Software in its entirety and without modifications,
 you must retain this notice and the following text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc.  may be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.
 Except as expressly stated in this notice, no other rights or licenses, express or implied, are granted by Apple herein,
 including but not limited to any patent rights that may be infringed by your derivative works or by other works in which the Apple Software may be incorporated.

 The Apple Software is provided by Apple on an "AS IS" basis.
 APPLE MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 REGARDING THE APPLE SOFTWARE OR ITS USE AND OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.

 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF CONTRACT,
 TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 Copyright (C) 2004-7 Apple Inc. All Rights Reserved.
"""

 As for my changes/additions to the code, as long as you don't restrict their free redistribution, do whatever you want with them, I won't be liable for anything.
*/


#import "Cocoa/Cocoa.h"


//=============================================================================================================== cWeapon


@interface  cWeapon : NSObject

    @property  (retain)  NSString*  name; // (retain) is OK here, we won't change that string afterwards in this example, but YMMV if you modify things...
    @property  int  damage;

    - initWithName: (NSString*)iName  damage: (int)iDamage;

@end // cWeapon


@implementation  cWeapon

- initWithName: (NSString*)iName  damage: (int)iDamage
{
    if( self = [super  init] )
    {
        self.name = iName;
        self.damage = iDamage;
    }
    return  self;
}

@end // cWeapon


//=============================================================================================================== cCombatant


@interface  cCombatant : NSObject

    @property  (retain)  NSString*  name; // (retain) is OK here, we won't change that string afterwards in this example, but YMMV if you modify things...
    @property  (assign)  cWeapon*  weapon1; // CHECK: (assign) is only OK if these can't be changed, which is the case in this demo, but probably not in the real world...
    @property  (assign)  cWeapon*  weapon2;
    @property  (assign)  cWeapon*  weapon3;
    @property  (assign)  cWeapon*  selectedWeapon;
    @property  int  shieldRating;


    - (unsigned int) countOfWeapons;
    - (id) objectInWeaponsAtIndex: (unsigned int)iIndex;

    //DEBUG
    - (void) addObserver: (NSObject*)iObserver
              forKeyPath: (NSString*)iKeyPath
                 options: (NSKeyValueObservingOptions)iOptions
                 context: (void*)iContext;

    //DEBUG
    - (void) removeObserver: (NSObject*)iObserver
                 forKeyPath: (NSString*)iKeyPath;

@end // cCombatant


@implementation  cCombatant


- init
{
    if( self = [super  init] )
    {
        // This simply sets up some defaults for the demo application
        self.weapon1 = [[cWeapon  alloc]  initWithName: @"Dagger" damage: 1];
        self.weapon2 = [[cWeapon  alloc]  initWithName: @"Sword"  damage: 3];
        self.weapon3 = [[cWeapon  alloc]  initWithName: @"Pike"   damage: 6];
//        [self  setSelectedWeapon: self.weapon1];//TEST:
    }
    return  self;
}


- (void) dealloc
{
    [self.weapon1  autorelease], self.weapon1 = nil;
    [self.weapon2  autorelease], self.weapon2 = nil;
    [self.weapon3  autorelease], self.weapon3 = nil;

    [super  dealloc];
}


- (unsigned int) countOfWeapons 
{
    // Fix this to the number of weapons allowed
    // Should be more intelligent about this in a full implementation
    return  3;
}


- (id) objectInWeaponsAtIndex: (unsigned int)iIndex 
{
    // Return the appropriate weapon for the specified index
    switch( iIndex )
    {
        case  0:  return  self.weapon1;
        case  1:  return  self.weapon2;
        case  2:  return  self.weapon3;
        default:  return  nil;
    }
    return  nil;
}


//DEBUG
- (void) addObserver: (NSObject*)iObserver
          forKeyPath: (NSString*)iKeyPath
             options: (NSKeyValueObservingOptions)iOptions
             context: (void*)iContext
{
    printf( "[cCombatant(%p) addObserver: %p forKeyPath: %s options: ... context: %p]\n", self, iObserver, [iKeyPath  UTF8String], /*iOptions,*/ iContext );
    int  bkpt=bkpt;
    [super  addObserver: iObserver  forKeyPath: iKeyPath  options: iOptions  context: iContext];
}


//DEBUG
- (void) removeObserver: (NSObject*)iObserver
             forKeyPath: (NSString*)iKeyPath
{
    printf( "[cCombatant(%p) removeObserver: %p forKeyPath: %s]\n", self, iObserver, [iKeyPath  UTF8String] );
    int  bkpt=bkpt;
    [super  removeObserver: iObserver  forKeyPath: iKeyPath];
}

@end // cCombatant


//=============================================================================================================== cCombatantsDelegate


@interface  cCombatantsDelegate : NSObject < NSTableViewDelegate >

    - (cCombatantsDelegate*) init;

@end // cCombatantsDelegate


@implementation  cCombatantsDelegate


- (cCombatantsDelegate*) init
{
    self = [super  init];
    // yeah I know... does nothing... but while debugging it's sometimes useful to insert code here
    return  self;
}


- (NSView*) tableView: (NSTableView*)iTableView 
   viewForTableColumn: (NSTableColumn*)iTableColumn 
                  row: (NSInteger)iRow
{
    NSTextField*  text = 0;
    NSTableCellView*  view = [iTableView  makeViewWithIdentifier: [iTableColumn  identifier]  owner: iTableView]; //CHECK: no NIB, what should the owner be ?
    if( !view )
    {
        view = [[NSTableCellView  alloc]  init];
        text = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 0, 0, 100, 17 )];
        [view  setTextField: text];
        [view  addSubview: text];
        [text  release];
        [view  autorelease];
    }
    else
    {
        //CHECK: why do we never get a recycled view ?
        int  bkpt=bkpt;
        text = [view  textField];
    }

    [text  setAllowsEditingTextAttributes: NO];
    [text  setBezeled: NO];
    [text  setBordered: NO];
    [text  setDrawsBackground: NO];
    [text  bind: @"value"  toObject: view  withKeyPath: @"objectValue.name"  options: 0];

    return  view;
}


//DEBUG:
- (BOOL) tableView: (NSTableView*)iTableView 
   shouldSelectRow: (NSInteger)iRow
{
    int  bkpt=bkpt;
    return  YES;  // this IS called (as well as others like this one), so why doesn't the selection go all the way to the controller ?!?!??!?!?!?!?
}


@end // cCombatantsDelegate


//=============================================================================================================== cTargetsDelegate


@interface  cTargetsDelegate : NSObject < NSTableViewDelegate >

    - (cTargetsDelegate*) init;

@end // cTargetsDelegate


@implementation  cTargetsDelegate


- (cTargetsDelegate*) init
{
    self = [super  init];
    // yeah I know... does nothing... but while debugging it's sometimes useful to insert code here
    return  self;
}


- (NSView*) tableView: (NSTableView*)iTableView 
   viewForTableColumn: (NSTableColumn*)iTableColumn 
                  row: (NSInteger)iRow
{
    NSTextField*  text = 0;
    NSTableCellView*  view = [iTableView  makeViewWithIdentifier: [iTableColumn  identifier]  owner: iTableView]; //CHECK: no NIB, what should the owner be ?
    if( !view )
    {
        view = [[NSTableCellView  alloc]  init];
        text = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 0, 0, 100, 17 )];
        [view  setTextField: text];
        [view  addSubview: text];
        [text  release];
        [view  autorelease];
    }
    else
    {
        //CHECK: why do we never get a recycled view ?
        int  bkpt=bkpt;
        text = [view  textField];
    }

    [text  setAllowsEditingTextAttributes: NO];
    [text  setBezeled: NO];
    [text  setBordered: NO];
    [text  setDrawsBackground: NO];
    if( [[iTableColumn  identifier]  isEqual: @"name"] )
    {
        [text  bind: @"value"  toObject: view  withKeyPath: @"objectValue.name"  options: 0];
    }
    else if( [[iTableColumn  identifier]  isEqual: @"shield"] )
    {
        [text  bind: @"value"  toObject: view  withKeyPath: @"objectValue.shieldRating"  options: 0];
    }

    return  view;
}


@end // cTargetsDelegate


//=============================================================================================================== cTargetsController


@interface  cTargetsController : NSArrayController
{
    NSArray*  selectedCombatants;  //TODO: property
}

    - (NSArray*) selectedCombatants;
    - (void) setSelectedCombatants: (NSArray*)iSelectedCombatants;

@end // cTargetsController


@implementation  cTargetsController

/*
 Create and return an array of all the combatants that are not selected
 */
- (NSArray *) arrangeObjects: (NSArray*)iObjectsToArrange
{
    printf("Targets::ArrangeObject()\n");
    unsigned int  scCount = (unsigned int)[selectedCombatants  count];
    
    if( (scCount == 0)  ||  (selectedCombatants == nil) )
        // second test effectively redundant, but...
    {
        printf("    no objects to arrange\n");
        return  [super  arrangeObjects: iObjectsToArrange];   
    }
    
    /*
     Create and return an array of all the combatants that are not selected
     */
    NSMutableArray*  arrangedObjects = [NSMutableArray  arrayWithCapacity: [iObjectsToArrange  count] - scCount];
    
    NSEnumerator*  objectEnumerator = [iObjectsToArrange  objectEnumerator];
    id  item;
    while( item = [objectEnumerator nextObject] )
    {
        if (![selectedCombatants  containsObject: item])
        {
            printf("    %s\n", [[item  name]  UTF8String]);
            [arrangedObjects  addObject: item];
        }
    }
    return  [super  arrangeObjects: arrangedObjects];
}


- (NSArray*) selectedCombatants
{
    return  selectedCombatants;
}


- (void) setSelectedCombatants: (NSArray*)iSelectedCombatants
{
    if( selectedCombatants != iSelectedCombatants )
    {
        [selectedCombatants  autorelease];
        selectedCombatants = [iSelectedCombatants  copy];
        [self  rearrangeObjects];
    }
}


- (void) dealloc
{
    [selectedCombatants  autorelease];
    [super  dealloc];
}


//TESTS:
- (BOOL) setSelectionIndex: (NSUInteger)iIndex
{
    BOOL  result = [super  setSelectionIndex: iIndex];
    printf("cTargetsController::setSelectionIndex: %d => %s\n",(int)iIndex,result?"true":"false");
    return  result;
}


- (BOOL) setSelectedObjects: (NSArray*)iObjects
{
    BOOL  result = [super  setSelectedObjects: iObjects];
    printf("cTargetsController::setSelectedObjects: %d => %s\n",(int)[iObjects  count],result?"true":"false");
    return  result;
}


@end // cTargetsController


//=============================================================================================================== MakeLabel()


// prototypes to shut up the compiler :-P
NSTextField*  MakeLabel( NSView*  iSuperView, NSString*  iLabel,  NSRect  iFrame );
NSTextField*  MakeLabelRight( NSView*  iSuperView, NSString*  iLabel,  NSRect  iFrame );

NSTextField*
MakeLabel( NSView*  iSuperView, NSString*  iLabel, NSRect  iFrame )
{
    NSTextField*  textfield = [[NSTextField  alloc]  initWithFrame: iFrame];
    [textfield  setEditable: NO];
    [textfield  setBezeled: NO];
    [textfield  setBordered: NO];
    [textfield  setDrawsBackground: NO];
    [textfield  setStringValue: iLabel];
    [iSuperView  addSubview: textfield];
    [textfield  release];

    return  textfield;
}


NSTextField*
MakeLabelRight( NSView*  iSuperView, NSString*  iLabel, NSRect  iFrame )
{
    NSTextField*  textfield = MakeLabel( iSuperView, iLabel, iFrame );
    [textfield  setAlignment: NSTextAlignmentRight];
    
    return  textfield;
}


//=============================================================================================================== cMyWindow


@interface  cMyWindow : NSWindow

    @property (assign)  NSArrayController*  combatantsController;
    @property (assign)  NSArrayController*  weaponController;
    @property (assign)  NSArrayController*  targetsController;
    @property (assign)  NSMutableArray*  combatants;
    @property (assign)  NSTextView*  battleHistory;

    - (cMyWindow*) initWithContentRect: (NSRect)iRect;
    - (void) buildGUI;

    - (void) appendToBattleHistory: (NSString*)iStringToAppend;
    - (void) attack;

@end // cMyWindow


@implementation  cMyWindow


- (cMyWindow*) initWithContentRect: (NSRect)iRect
{
    self = [super  initWithContentRect: NSMakeRect( 50, 50, 595, 578 )
                             styleMask: NSWindowStyleMaskTitled
                               backing: NSBackingStoreBuffered
                                 defer: NO
            ];

    self.combatants = [[NSMutableArray  alloc]  init];
    for( int  i = 0; i < 25; ++i )
    {
        cCombatant*  combatant = [[cCombatant  alloc]  init];
        [self.combatants  addObject: combatant];
        [combatant  setName: [NSString  stringWithFormat: @"Combatant %d", i+1]];
        [combatant  release];
    }

    printf("combatants = %p\n", self.combatants);

    self.combatantsController = [[NSArrayController  alloc]  initWithContent: self.combatants];
    [self.combatantsController  setSelectionIndex: 1]; // Vlad
    printf("combatantsController = %p\n", self.combatantsController);

    self.weaponController = [[NSArrayController  alloc]  init];
    [self.weaponController  bind: @"contentArray"  toObject: self.combatantsController  withKeyPath: @"selection.weapons"  options: 0];
    printf("weaponController = %p\n", self.weaponController);

    self.targetsController = [[cTargetsController  alloc]  initWithContent: self.combatants];
    [self.targetsController  bind: @"selectedCombatants"  toObject: self.combatantsController  withKeyPath: @"selectedObjects"  options: nil];
    printf("targetsController = %p\n", self.targetsController);

    self.battleHistory = nil;

    return  self;
}


- (void) buildGUI
{
    NSTextField*  textfield = 0;

//TODO: right now we hardcode positions, later redo it with layout/constraints
//TODO: release delegates

// these weird comment lines are the output of [[window  contentView]  _subtreeDescription] in the original 2007 code...

//[ D A       LU ] h=--- v=--- NSView 0x630000120000 f=(0,0,595,578) b=(-) TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
    cMyWindow*  window = self;
    printf("Binding window title to selection.name\n");
    [window  bind: @"title"  toObject: self.combatantsController  withKeyPath: @"selection.name"  options: 0];
    NSView*  view = [window  contentView];

//  [   AF      LU ] h=--& v=&-- NSTextField 0x6380001e0000 "Combatants" f=(22,541,131,17) b=(-) TIME drawRect: min/mean/max 0.14/0.14/0.14 ms
    MakeLabel( view, @"Combatants",  NSMakeRect( 22, 541, 131, 17 ) );

//  [   AF      LU ] h=--& v=&-- NSTextField 0x6500001e0700 "Selected Attacker" f=(265,541,201,17) b=(-) TIME drawRect: min/mean/max 0.26/0.26/0.26 ms
    MakeLabel( view, @"Selected Attacker",  NSMakeRect( 265, 541, 201, 17 ) );

//  [   AF O   WlU#] h=--- v=--- NSScrollView 0x6500001c02d0 f=(25,386,218,147) b=(-) => <_NSViewBackingLayer: 0x620000041470> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF O   wlU ] h=-&- v=-&- NSClipView 0x10a308380 f=(1,0,201,131) b=(0,-23,-,-) => <_NSClipViewBackingLayer: 0x620000040a20> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF O   wLU ] h=--- v=--- NSTableView 0x10a308690 f=(0,0,201,114) b=(-) => <_NSViewBackingLayer: 0x620000040bd0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=--- v=--- NSScroller 0x6500001c03c0 f=(202,23,15,108) b=(-) => <_NSViewBackingLayer: 0x620000040990> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=--- v=--- NSScroller 0x6500001c04b0 f=(1,131,201,15) b=(-) => <_NSViewBackingLayer: 0x6200000407b0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=-&- v=--& NSClipView 0x10a308d30 f=(1,0,201,23) b=(-) => <_NSClipViewBackingLayer: 0x620000040ae0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   A      wlU ] h=--- v=--- NSVisualEffectView 0x100100a90 f=(0,0,201,23) b=(-) => <_NSViewBackingLayer: 0x6200000411a0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF     wLU ] h=--- v=--- NSTableHeaderView 0x6500001a0620 f=(0,0,201,23) b=(-) => <_NSViewBackingLayer: 0x620000040ea0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wLU ] h=--- v=--- _NSCornerView 0x650000120460 f=(202,0,16,23) b=(-) => <_NSViewBackingLayer: 0x6200000415c0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
    NSScrollView*  combatantsScroll = [[NSScrollView  alloc]  initWithFrame: NSMakeRect( 25, 386, 218, 147 )];
    [combatantsScroll  setHasVerticalScroller: YES];
    [combatantsScroll  setHasHorizontalScroller: YES];
    [combatantsScroll  setBorderType: NSLineBorder];
    NSTableView*  combatantsTable = [[NSTableView  alloc]  initWithFrame: NSMakeRect( 0, 0, 150, 100 )];// I guess frame is ignored... UPDATE: not totally ignored !!! setting frame too big will cause strange things with the header !
    [combatantsTable  setDelegate: [[cCombatantsDelegate  alloc]  init]];
    
    NSTableColumn*  combatantsNameColumn = [[NSTableColumn  alloc]  initWithIdentifier: @"name"];
    [combatantsNameColumn  setTitle: @"Name"];
    [combatantsTable  addTableColumn: combatantsNameColumn];
    [combatantsNameColumn  release];

    printf("Binding table to arrangedObjects\n");
    [combatantsTable  bind: @"content"  toObject: self.combatantsController  withKeyPath: @"arrangedObjects"  options: 0];
    
    [combatantsScroll  setDocumentView: combatantsTable];
    [combatantsTable  release];
    [[window  contentView]  addSubview: combatantsScroll];
    [combatantsScroll  release];

//  [   AF      LU ] h=-&- v=&-- NSTextField 0x6300001e0000 "Combatant 1" f=(268,511,307,22) b=(-) TIME drawRect: min/mean/max 0.57/1.31/2.05 ms
    textfield = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 268, 511, 307, 22 )];
    [textfield  bind: @"value"  toObject: self.combatantsController  withKeyPath: @"selection.name"  options: 0];
    [view  addSubview: textfield];
    [textfield  release];

//  [   AF      LU ] h=--- v=--- NSTextField 0x6380001e0200 "Weapon:" f=(265,485,61,17) b=(-) TIME drawRect: min/mean/max 0.24/0.24/0.24 ms
    MakeLabelRight( view, @"Weapon:",  NSMakeRect( 265, 485, 61, 17 ) );

//  [   AF      LU ] h=--- v=--- NSPopUpButton 0x638000160780 "No Value" f=(328,479,169,26) b=(-) TIME drawRect: min/mean/max 1.13/1.59/2.05 ms
    NSPopUpButton*  popup = [[NSPopUpButton  alloc]  initWithFrame: NSMakeRect( 328, 479, 169, 26 )  pullsDown: NO];
    [popup  bind: @"content"         toObject: self.weaponController      withKeyPath: @"arrangedObjects"           options: 0];
    [popup  bind: @"contentValues"   toObject: self.weaponController      withKeyPath: @"arrangedObjects.name"      options: 0];
    [popup  bind: @"selectedObject"  toObject: self.combatantsController  withKeyPath: @"selection.selectedWeapon"  options: 0];
    [[window  contentView]  addSubview: popup];
    [popup  release];

//  [   AF      LU ] h=--- v=--- NSTextField 0x6500001e0100 "Damage:" f=(252,443,74,17) b=(-) TIME drawRect: min/mean/max 0.21/0.21/0.21 ms
    MakeLabelRight( view, @"Damage:",  NSMakeRect( 252, 443, 74, 17 ) );

//  [   AF      LU ] h=--- v=--- NSTextField 0x6500001e0200 f=(328,441,66,22) b=(-) TIME drawRect: min/mean/max 0.43/0.43/0.43 ms
    textfield = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 328, 441, 66, 22 )];
    [textfield  bind: @"value"  toObject: self.combatantsController  withKeyPath: @"selection.selectedWeapon.damage"  options: 0];
    [view  addSubview: textfield];
    [textfield  release];

//--

//  [   AF      LU ] h=--- v=--- NSTextField 0x6500001e0000 "Targets" f=(22,352,78,17) b=(-) TIME drawRect: min/mean/max 0.20/0.20/0.20 ms
    MakeLabel( view, @"Targets",  NSMakeRect( 22, 352, 78, 17 ) );

//  [   AF      LU ] h=--& v=&-- NSTextField 0x6500001e0800 "Selected Target" f=(264,352,201,17) b=(-) TIME drawRect: min/mean/max 0.10/0.10/0.10 ms
    MakeLabel( view, @"Selected Target",  NSMakeRect( 264, 352, 201, 17 ) );

//  [   AF O   WlU#] h=--- v=--- NSScrollView 0x6500001c0000 f=(25,194,218,150) b=(-) => <_NSViewBackingLayer: 0x600000040ff0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF O   wlU ] h=-&- v=-&- NSClipView 0x10a301560 f=(1,0,201,134) b=(0,-23,-,-) => <_NSClipViewBackingLayer: 0x608000043000> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF O   wLU ] h=--- v=--- NSTableView 0x10a303350 f=(0,0,201,111) b=(-) => <_NSViewBackingLayer: 0x608000040ab0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=--- v=--- NSScroller 0x6500001c00f0 f=(202,23,15,111) b=(-) => <_NSViewBackingLayer: 0x608000043f00> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=--- v=--- NSScroller 0x6500001c01e0 f=(1,134,201,15) b=(-) => <_NSViewBackingLayer: 0x608000044590> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=-&- v=--& NSClipView 0x10a303e20 f=(1,0,201,23) b=(-) => <_NSClipViewBackingLayer: 0x608000044410> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   A      wlU ] h=--- v=--- NSVisualEffectView 0x100407d70 f=(0,0,201,23) b=(-) => <_NSViewBackingLayer: 0x6080000451c0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF     wLU ] h=--- v=--- NSTableHeaderView 0x6500001a0380 f=(0,0,201,23) b=(-) => <_NSViewBackingLayer: 0x608000044050> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wLU ] h=--- v=--- _NSCornerView 0x6500001201e0 f=(202,0,16,23) b=(-) => <_NSViewBackingLayer: 0x6080000452b0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
    printf("Building targets\n");
    NSScrollView*  targetsScroll = [[NSScrollView  alloc]  initWithFrame: NSMakeRect( 25, 194, 218, 150 )];
    [targetsScroll  setHasVerticalScroller: YES];
    [targetsScroll  setHasHorizontalScroller: YES];
    [targetsScroll  setBorderType: NSLineBorder];
    NSTableView*  targetsTable = [[NSTableView  alloc]  initWithFrame: NSMakeRect( 0, 0, 150, 100 )];
    [targetsTable  setDelegate: [[cTargetsDelegate  alloc]  init]];

    NSTableColumn*  targetsNameColumn = [[NSTableColumn  alloc]  initWithIdentifier: @"name"];
    [targetsNameColumn  setTitle: @"Name"];
    [targetsTable  addTableColumn: targetsNameColumn];
    [targetsNameColumn  release];
    NSTableColumn*  targetsShieldColumn = [[NSTableColumn  alloc]  initWithIdentifier: @"shield"];
    [targetsShieldColumn  setTitle: @"Shield"];
    [targetsTable  addTableColumn: targetsShieldColumn];
    [targetsShieldColumn  release];

    printf("Binding targetsTable to arrangedObjects\n");
    [targetsTable  bind: @"content"  toObject: self.targetsController  withKeyPath: @"arrangedObjects"  options: 0];

    [targetsScroll  setDocumentView: targetsTable];
    [targetsTable  release];
    [[window  contentView]  addSubview: targetsScroll];
    [targetsScroll  release];

//  [   AF      LU ] h=-&- v=&-- NSTextField 0x6500001e0400 f=(267,322,307,22) b=(-) TIME drawRect: min/mean/max 0.63/0.63/0.63 ms
    textfield = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 267, 322, 307, 22 )];
    [textfield  bind: @"value"  toObject: self.targetsController  withKeyPath: @"selection.name"  options: 0];
    [view  addSubview: textfield];
    [textfield  release];

//  [   AF      LU ] h=--- v=--- NSTextField 0x6500001e0600 "Shield Rating:" f=(264,294,93,17) b=(-) TIME drawRect: min/mean/max 0.24/0.24/0.24 ms
    MakeLabelRight( view, @"Shield Rating:",  NSMakeRect( 264, 294, 93, 17 ) );

//  [   AF      LU ] h=--- v=--- NSTextField 0x6500001e0500 f=(359,292,91,22) b=(-) TIME drawRect: min/mean/max 0.47/0.47/0.47 ms
    textfield = [[NSTextField  alloc]  initWithFrame: NSMakeRect( 359, 292, 91, 22 )];
    [textfield  bind: @"value"  toObject: self.targetsController  withKeyPath: @"selection.shieldRating"  options: 0];
    [view  addSubview: textfield];
    [textfield  release];

//  [ D AF      LU ] h=--- v=--- NSButton 0x650000140210 "Attack" f=(471,186,109,32) b=(-) TIME drawRect: min/mean/max 0.67/0.79/0.91 ms
    NSButton*  attack = [[NSButton  alloc]  initWithFrame: NSMakeRect( 471, 186, 109, 32 )];
    [attack  setTitle: @"Attack"];
    [attack  setButtonType:  NSButtonTypeMomentaryPushIn];
    [attack  setBezelStyle:  NSRoundedBezelStyle];
    [attack  setTarget: window];
    [attack  setAction: @selector(attack)];
    [view  addSubview: attack];
    [attack  release];

//  [   A     P LU ] h=--- v=--- NSBox 0x10a309bd0 "Box" f=(25,176,549,5) b=(-) TIME drawRect: min/mean/max 0.05/0.05/0.05 ms
    NSBox*  separator = [[NSBox alloc]  init];
    [separator  setFrame: NSMakeRect( 25, 176, 549, 5 )];
    [separator  setBoxType: NSBoxSeparator];
    [view  addSubview: separator];
    [separator  release];

//  [   AF      LU ] h=--& v=&-- NSTextField 0x6500001e0900 "Battle history" f=(22,153,201,17) b=(-) TIME drawRect: min/mean/max 0.18/0.18/0.18 ms
    MakeLabel( view, @"Battle history",  NSMakeRect( 22, 153, 201, 17 ) );

//  [   AF O   WlU#] h=-&- v=--& NSScrollView 0x6500001c05a0 f=(25,20,549,125) b=(-) => <_NSViewBackingLayer: 0x600000041470> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF O   wlU ] h=-&- v=-&- NSClipView 0x10a30b470 f=(1,1,532,123) b=(-) => <_NSClipViewBackingLayer: 0x600000041830> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF     wLU ] h=--- v=--- _NSClipViewOverhangView 0x600000120460 f=(0,123,532,256) b=(-) => <_NSViewBackingLayer: 0x600000041950> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF     wLU ] h=--- v=--- _NSClipViewOverhangView 0x6000001200a0 f=(0,-256,532,256) b=(-) => <_NSViewBackingLayer: 0x6000000414d0> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//      [   AF O   wLU ] h=-&- v=-&- NSTextView 0x650000120500 f=(0,0,532,123) b=(-) => <_NSTextViewBackingLayer: 0x600000064180> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [   AF     wlU ] h=--- v=--- NSScroller 0x6180001c0000 f=(533,1,15,123) b=(-) => <_NSViewBackingLayer: 0x600000041530> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
//    [  HAF     wlU ] h=--- v=--- NSScroller 0x6180001c00f0 f=(-100,-100,87,18) b=(-) => <_NSViewBackingLayer: 0x600000041980> TIME drawRect: min/mean/max 0.00/0.00/0.00 ms
    NSScrollView*  historyScroll = [[NSScrollView  alloc]  initWithFrame: NSMakeRect( 25, 20, 549, 125 )];
    [historyScroll  setHasVerticalScroller: YES];
    [historyScroll  setBorderType: NSLineBorder];
    self.battleHistory = [[NSTextView  alloc]  initWithFrame: NSMakeRect( 0, 0, 500, 100 )];

    [historyScroll  setDocumentView: self.battleHistory];
    [self.battleHistory  release];
    [[window  contentView]  addSubview: historyScroll];
    [historyScroll  release];

// GUI done...

#if 0
    NSLog(@"%@", [[window  contentView]  _subtreeDescription]);
    NSLog(@"subviews # %d", (int)[[combatantsTable  subviews]  count]);
    for( NSView*  iview  in  [combatantsTable  subviews] )
    {
        NSLog(@"%@", [iview  _subtreeDescription]);
    }
#endif
}


- (void) appendToBattleHistory: (NSString*)iStringToAppend
{
    int  textLength = (int)[[self.battleHistory  textStorage]  length];
    NSRange  range = NSMakeRange(textLength, 0);
    
    [self.battleHistory  replaceCharactersInRange:range  withString:iStringToAppend];
    
    textLength = (int)[[self.battleHistory  textStorage]  length];
    range = NSMakeRange( textLength, 0 );
    [self.battleHistory  scrollRangeToVisible: range];
}


- (void) attack
{
    printf("ATTACK\n");

    BOOL  okToBattle = YES;

    NSString*  string = 0;

    NSArray*  selectedCombatants = [self.combatantsController  selectedObjects];


    if( [selectedCombatants  count] == 0 )
    {
        string = @"No attacker selected.\n";
        [self  appendToBattleHistory: string];
        okToBattle = NO;
    }

    id  attacker = [selectedCombatants  objectAtIndex: 0];
    id  weapon = [attacker  selectedWeapon];
    
    if( weapon == nil )
    {
        string = [NSString  stringWithFormat: @"%@ doesn't have a weapon.\n", [attacker  name] ];
        [self  appendToBattleHistory: string];
        okToBattle = NO;
    }

    NSArray*  selectedTargets = [self.targetsController  selectedObjects];	
    
    if( [selectedTargets  count] == 0 )
    {
        string = @"No defender selected.\n";
        [self  appendToBattleHistory: string];
        okToBattle = NO;
    }

    if( !okToBattle )
    {
        [self  appendToBattleHistory: @"\n"];
        return;
    }

    id  defender = [selectedTargets  objectAtIndex: 0];
    
    string = [NSString  stringWithFormat: @"%@ attacks %@ with %@.\n\n", [attacker  name], [defender  name], [weapon  name]];
    [self  appendToBattleHistory: string];
}


@end // cMyWindow


//=============================================================================================================== JustDoIt()


void
JustDoIt()
{
    @try
    {
        cMyWindow*  window = [[cMyWindow  alloc]  initWithContentRect: NSMakeRect( 50, 50, 595, 578 )];
        [window  buildGUI];
        [window  makeKeyAndOrderFront: 0];
    }
    @catch( NSException* e )
    {
        int  bkpt=bkpt;
    }
}

